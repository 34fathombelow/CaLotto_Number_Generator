# California Lottery Number Generator

### A simple python script that generates California Lottery numbers for various draw based games.
During the COVID-19 pandemic I started my journey to learn python.  This is my first attempt of writing something completely on my own.  I know it is far from perfect.  All constructive feedback is welcome.