from lottogames import Lotto
import os

#  game_name, num_of_balls, range_of_balls, num_of_power, range_of_power
powerball = Lotto('PowerBall', 5, 69, 1, 26)
megamillions = Lotto('MegaMillions', 5, 70, 1, 25)
superlotto = Lotto('SuperLotto', 5, 47, 1, 27)
fantasy5 = Lotto('Fantasy5', 5, 39, 0, 0)
daily3 = Lotto('Daily3', 3, 9, 0, 0)
daily4 = Lotto('Daily4', 4, 9, 0, 0)


def All_games():
    powerball.power_game()
    megamillions.power_game()
    superlotto.power_game()
    fantasy5.power_game()
    daily4.daily_game()
    daily3.daily_game()


def Main_menu():
    print("Please choose a Lotto game:")
    for index, list in enumerate(Lotto.game_list, start=1):
        print(f"{index}. {list}")
    print("7. Play all games")
    print("0. Quit")
    try:
        choice = int(input("Enter a choice: "))
        if choice == 1:
            powerball.power_game()
        elif choice == 2:
            megamillions.power_game()
        elif choice == 3:
            superlotto.power_game()
        elif choice == 4:
            fantasy5.power_game()
        elif choice == 5:
            daily3.daily_game()
        elif choice == 6:
            daily4.daily_game()
        elif choice == 7:
            All_games()
        elif choice == 0:
            quit()
        else:
            os.system('clear')
            print("Invalid Choice. Enter a valid number")
            Main_menu()
    except ValueError:
        os.system('clear')
        print("Invalid Choice. Enter a valid number")
        Main_menu()


Main_menu()
