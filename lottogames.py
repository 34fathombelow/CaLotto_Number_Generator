import random

class Lotto():

    game_list = []

    def __init__(self, game_name, num_of_balls, range_of_balls, num_of_power, range_of_power):
        self.game_name = game_name
        self.num_of_balls = num_of_balls
        self.range_of_balls = range_of_balls
        self.num_of_power = num_of_power
        self.range_of_power = range_of_power
        Lotto.game_list.append(self.game_name)

    def power_game(self):
        numbers = []
        power_number = []
        for i in range(0, self.num_of_balls):
            number = random.randint(1, self.range_of_balls)
            while number in numbers:
                number = random.randint(1, self.range_of_balls)
            numbers.append(number)

        if self.num_of_power == 0:
            print(f"{self.game_name} numbers: {numbers}")
        else:
            number = random.randint(1, self.range_of_power)
            while number in numbers:
                number = random.randint(1, self.range_of_power)
            power_number.append(number)
            print(f"{self.game_name} numbers: {numbers} {power_number}")

    def daily_game(self):
        numbers = []
        for i in range(0, self.num_of_balls):
            number = random.randint(1, self.range_of_balls)
            numbers.append(number)
        print(f"{self.game_name} numbers: {numbers}")
